#include "Background.h"



Background::Background(Point2f initialPosition, char* sourceImage, SDL_Renderer* renderer)
{

	backSprite.SetText(renderer, sourceImage);
}


Background::~Background()
{
}

void Background::update(KeyFlags keys, int framerate)
{
}

void Background::draw(SDL_Renderer * renderer)
{
	SDL_Rect targetRect;

	// Derived value
	targetRect.x = 100;
	targetRect.y = 100;
	targetRect.w = 200; // player size on-screen


	float hwRatio = backSprite.GetRect().h / backSprite.GetRect().w;
	targetRect.h = targetRect.w  * hwRatio;
	SDL_RenderCopyEx(renderer, backSprite.GetText(),NULL, NULL, 0, 0, SDL_FLIP_HORIZONTAL);
}
