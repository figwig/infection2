#include "Bullet.h"



Bullet::Bullet(Point2f initialPosition, char* sourceImage, SDL_Renderer* renderer, float angle)
{

	theta2 = ((-angle)*3.14) / 180;
 	pos = Point2f(initialPosition.x -(100*sin(theta2)), initialPosition.y-(100 * cos(theta2)));
	bulletSprite.SetText(renderer, sourceImage);
	theta = angle;
	SDL_Rect targetRect;

	// Derived value
	targetRect.x = int(pos.x);
	targetRect.y = int(pos.y);
	targetRect.w = bulletSprite.GetRect().w * SIZE;
	targetRect.h = bulletSprite.GetRect().h * SIZE;
	deleteMe = false;

	bulletVolume._init_(targetRect.w,targetRect.h, pos,true , theta);

	bulletVolume.setOwner(this);

	printf("Created bullet!\n");

}


Bullet::~Bullet()
{

}

void Bullet::flipvelocity(GameObject* perp)
{
	
	age += perp->getDamageBullet()*framerate;
}

void Bullet::update(KeyFlags keys, int newFramerate)
{
	framerate = newFramerate;
	theta2 = ((-theta)*3.14)/180;

	float xMove = (SPEED / framerate )*sin(theta2);
	float yMove = (SPEED / framerate)*cos(theta2);

	pos.x -= xMove;
	pos.y -= yMove;
	age += 1;

	if (age > 4 * framerate)
	{
		deleteMe = true;
	}
}

void Bullet::draw(SDL_Renderer * renderer)
{

	SDL_Rect targetRect;

	// Derived value
	targetRect.x = int(pos.x);
	targetRect.y = int(pos.y);
	targetRect.w = bulletSprite.GetRect().w * SIZE;
	targetRect.h = bulletSprite.GetRect().h * SIZE;

	float x = (pos.x + ((bulletSprite.GetRect().w * SIZE) / 2.0f));
	float y = (pos.y - ((bulletSprite.GetRect().w * SIZE) / 2.0f));

	Point2f centrePos = Point2f(pos.x + targetRect.w/2, pos.y + targetRect.h/2);
	bulletVolume._init_(targetRect.w, targetRect.h, centrePos, false, theta);


	SDL_RenderCopyEx(renderer, bulletSprite.GetText(), &bulletSprite.GetRect(), &targetRect, theta, 0, SDL_FLIP_HORIZONTAL);
}
