#pragma once
#include "GameObject.h"
#include "Point2f.h"
#include "rectVol.h"
#include "Sprite.h"
class Bullet :
	public GameObject
{

private:
	Point2f				pos; // the bullet's position in the world

	float				theta;
	float theta2;

	Sprite				bulletSprite;
	int framerate;
	const float			SIZE = 0.05f;
	const float			SPEED = 600.0f;

	rectVol			bulletVolume;

	int age = 0;
public:

	Bullet(Point2f initialPosition, char* sourceImage, SDL_Renderer* renderer, float);
	~Bullet();
	void update(KeyFlags keys, int framerate);

	// Draw the player - we call this every iteration of the main game loop
	void draw(SDL_Renderer* renderer);

	Point2f getPos() { return pos; }
	Volume* getVolumePtr() { return (Volume*) &bulletVolume; }

	int getDamagePlayer() { return 0; }
	int getDamageVirus() { return 20; }

	void flipvelocity(GameObject*);

	int getAge() { return age; }
};

