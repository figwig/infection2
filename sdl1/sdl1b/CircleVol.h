
#pragma once
#include "Volume.h"
#include "GameObject.h"
#include "Point2f.h"
#include "Collision.h"

class CircleVol : public Volume
{
public:
	CircleVol();
	~CircleVol();
	void _init_(float newradius, Point2f newcentre, bool ispassive);
	bool checkCollision(Point2f);
	Point2f*  getPoints();
	void createPoints(float);
	int getType();
	float getRadius();
	int getNumberOfPoints();
	void updatePos(int,int);
	void setOwner(GameObject*);
	GameObject* getOwner();

private:
	float radius;
	Point2f centre;
	bool passive;
	static const int NO_OF_POINTS = 100;
	Point2f keyPoints[NO_OF_POINTS] = { Point2f(0,0) };
	GameObject* owner;
	int actNumPoints;

};

