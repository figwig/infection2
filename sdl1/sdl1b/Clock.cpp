#include "Clock.h"

#include <SDL.h>
#include <stdio.h>

Clock::Clock()
{
	framerate = 0;
	avgFramerate = 0;
}


Clock::~Clock()
{
}

void Clock::tick(int tick, bool newdebugb)
{
	diff = 0.0f;
	float newTimer = 0.0f;

	debugb = newdebugb;
	if (tick == 0)//first two frames are an intialization phase
	{
		firstTimer = SDL_GetTicks();
	}
	else if (tick == 1)
	{
		secondTimer = SDL_GetTicks();
		diff = secondTimer - firstTimer;
		framerate = 1000.0f / diff;
	}

	else// routine tick
	{
		delayTillNextFrame();
		newTimer = SDL_GetTicks();
		diff = newTimer - lastTimer;

		framerate = 1000.0f / diff;
		avgFramerate = (framerate + avgFramerate) / 2.0f;
		// setting up last values to current ones for next run
		lastTimer = newTimer;
		lastFramerate = framerate;

		//timepassed calc
		timePassed += diff/1000;


	}

}

int Clock::getFramerate()
{
	if (framerate < 1 )
	{
		framerate = 1;
	}
	return framerate;
}

int Clock::getAvgFramerate()
{

	return avgFramerate;
}
float Clock::getTimePassed()
{//returns the time passed in seconds
	return timePassed;
}

void Clock::delayTillNextFrame()
{
	float ms = 1000.0f / FRAMERATETARGET;
	if (diff < ms)
	{
		float debug = 1.0f;
		if (debugb)
		{
			debug = 1.2f;
		}
		SDL_Delay((ms*debug) - diff);
	}

}
