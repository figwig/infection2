#include "Collision.h"
#include "Volume.h"


Collision::Collision(Volume* newPrimary, Volume* newSecondary, Point2f newPosition, float newSpeed)
{
	primary = newPrimary;
	secondary = newSecondary;
	position = newPosition;
	speed = newSpeed;
}


Collision::~Collision()
{
}

Volume * Collision::getPrimary()
{
	return (Volume*) primary;
}

Volume * Collision::getSecondary()
{
	return (Volume*) secondary;
}

Point2f Collision::getPos()
{
	return position;
}

float Collision::getSpeed()
{
	return 0.0f;
}
