#pragma once

#include "Point2f.h"
class Volume;

class Collision
{

public:
	Collision(Volume*, Volume* , Point2f, float);
	~Collision();
	Volume* getPrimary();
	Volume* getSecondary();
	Point2f getPos();
	float getSpeed();

private:
	Point2f position;
	Volume* primary;
	Volume* secondary;
	float speed;
};