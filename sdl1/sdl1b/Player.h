#pragma once

#include <SDL.h>
#include "KeyState.h"
#include "Point2f.h"
#include "Sprite.h"
#include "Volume.h"
#include "CircleVol.h"
#include "GameObject.h"
#include "rectVol.h"
#include "Bullet.h"
#include "staticText.h"
#include "Virus.h"

class GameScene;

class  Player : public GameObject {


private:


	//Constants
	const float SIZE = 0.5f;
	const float VOLUMEMOD = 0.9f;
	bool readyFire = true;

	//Bools
	bool debugb = false;

	//ints
	int health = 40;
	int bulletDelay = 0;

	//Motion related Floats
	float theta;			 //rotation
	float theta2 = 0.0f;	 //rotation in radians
	float maxSpeed = 0.0f;	 //maximum Speed per frame, calculated by framerate
	float decayMultiplier = 1.0f; //Decay multiplier
	float speed = 0.0f;      //the acceleration of the player 
	float currentScaled = 0.0f; //the current normalized speed
	float decay = 0.0f;		 //The decay value

	//Motion Related Point2fs
	Point2f pos;			 //position to be renderered at
	Point2f currentSpeed;    //players current speed	
	Point2f tempMove = Point2f(0.0f, 0.0f);

	//Objects
	rectVol playerVolume;	 //the collision detecting volume for Player
	Sprite playerSprite;	 //the player's Sprite
	staticText* debug;		 //the object drawn to add Co-ords in debug mode
	staticText* debug2;
	SDL_Renderer* renderer;  //the renderer to draw things on the screen
	SDL_Rect targetRect;     //The rectangle describing the size of player on screen
	GameScene* sceneRef = nullptr; // The address for the gameScene object - for adding objects

	//Various private Methods
	void Shoot();
	void Reload() { readyFire = true; }
	void move(int framerate, KeyFlags keys);
	void createMotion(int framerate, KeyFlags keys);
	float speedScale(Point2f, float);





public:
	//The Player Object's Constructor
	Player(Point2f initialPosition, char* sourceImage, SDL_Renderer*, GameScene*);
	~Player();

	void update(KeyFlags keys, int framerate);
	void draw(SDL_Renderer* renderer);
	void flipvelocity(GameObject*);
	void setHealth(int newHealth) { health = newHealth; }

	int getDamageVirus() { return 1; }
	int getDamagePlayer() { return 0; }
	int getDamageBullet() { return 0; }
	int getHealth() { return health; }

	Point2f getPos(void);

	Volume*  Player::getVolumePtr();
};