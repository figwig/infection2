#include "Player.h"
#include <SDL_image.h>
#include "Sprite.h"
#include "GameObject.h"
#include <SDL.h>
#include "KeyState.h"
#include "Point2f.h"
#include "Volume.h"
#include "CircleVol.h"
#include "Sprite.h"
#include "rectVol.h"
#include <cmath>
void Player::update(KeyFlags keys, int framerate) {

	// Move the player
	move(framerate, keys);
	tempMove.x = 0.0f;
	tempMove.y = 0.0f;
	//Shooting!
	if (keys& Keys::Space)
	{
		if (readyFire == true)
		{
			Shoot();
		}
	}
	else
	{
		if (readyFire == false)
		{
			Reload();
		}
	}
	//if movement  value is mathematically smaller than an int 
	if ((currentSpeed.x)*(currentSpeed.x) < 0.0001f)
	{
		currentSpeed.x = 0.0f;
	}
	if ((currentSpeed.y)*(currentSpeed.y) < 0.0001f)
	{
		currentSpeed.y = 0.0f;
	}



	//If moving, sprite is texture 2
	if ((currentSpeed.x != 0.0f)||(currentSpeed.y != 0.0f))
	{
		
		playerSprite.SetRect(0, 291, 173, 291);
	}
	else
	{
		playerSprite.SetRect(0, 0, 173, 291);
	}

	//get PosX and Y for debugging
	if (debugb)
	{
		debug->update(keys, playerVolume.getPoints()[0].x);
		debug2->update(keys, playerVolume.getPoints()[0].y);
	}

	//reduce the bulletDelay by 1
	if (bulletDelay > 0)
	{
		bulletDelay -= 1;
	}
}


void Player::move(int framerate, KeyFlags keys)
{
	speed = (2.5f / framerate);

	maxSpeed = (500.0f / framerate);

	decay = (70.0f / framerate);

	//Rotating the Player!
	if (keys & Keys::A)
	{
		theta -= 120.0f / framerate;
	}
	if (keys & Keys::D)
	{
		theta += 120.0f / framerate;
	}

	theta2 = ((-theta)*3.14) / 180;

	///Right movement
	if (keys & Keys::Right) {
		if (currentScaled < maxSpeed)
		{
			currentSpeed.x += speed;
		}
		else { currentSpeed.x = maxSpeed; }
	}
	//Right decay
	else
	{
		if (currentSpeed.x > 0.0f)
		{
			currentSpeed.x -= speed * decay;

		}
	}


	//left movement
	if (keys & Keys::Left) {

		if (currentScaled < maxSpeed)
		{
			currentSpeed.x -= speed;

		}
		else { currentSpeed.x = -maxSpeed; }
	}
	//left decay
	else
	{
		if (currentSpeed.x < 0.0f)
		{
			currentSpeed.x += speed * decay;

		}
	}



	//up movement
	if (keys & Keys::Up) {
		if (currentScaled < maxSpeed)
		{
			currentSpeed.y -= speed;

		}
		else { currentSpeed.y = -maxSpeed; }
	}
	//up decay
	else
	{
		if (currentSpeed.y < 0.0f)
		{
			currentSpeed.y += speed * decay;
		}
	}


	//down movement
	if (keys & Keys::Down) {
		if (currentScaled < maxSpeed)
		{
			currentSpeed.y += speed;

		}
		else { currentSpeed.y = maxSpeed; }

	}
	//down decay
	else
	{
		if (currentSpeed.y > 0.0f)
		{
			currentSpeed.y -= speed * decay;
		}
	}


	tempMove.x += currentSpeed.x * cos(-theta2);
	tempMove.y += currentSpeed.x * sin(-theta2);

	tempMove.x += currentSpeed.y * cos(theta2);
	tempMove.y += currentSpeed.y * sin(theta2);


	currentScaled = speedScale(tempMove, 0.0f);

	//takeaway amount = x+y-hypo. Take this away from each direction
	
	if (pow(currentScaled,2) > pow(maxSpeed,2))
	{
		if (decayMultiplier > 0.4f)
		{
			decayMultiplier -= 0.002f;
		}
	}
	else
	{
		if (decayMultiplier < 1.0f)
		{
			decayMultiplier += 0.05f;
		}

	}

	
	pos.x += currentSpeed.x * cos(-theta2)*decayMultiplier;
	pos.y += currentSpeed.x * sin(-theta2)*decayMultiplier;

	pos.y += currentSpeed.y * cos(theta2)*decayMultiplier;
	pos.x += currentSpeed.y * sin(theta2)*decayMultiplier;



}


void Player::createMotion(int framerate, KeyFlags keys)
{

}
//New Move!!

//Point2f momentum is the actual movement that is added to pos


//momentum tends to 0 everyframe, regardless of keypress!! Done using a mulitplier so that it is smooth

//Keypress increases the momentum in the correct direction - each if statement does it's own sin cos n that!

//