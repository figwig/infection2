#pragma once


//
// Model a single 2D point (x, y)
//

class Point2f {

public:

	float		x, y;

	// Constructors

	// Default constructor - initialise x and y to 0.0f
	Point2f();

	Point2f(const float initX, const float initY);


	// Note: Since Point2f contains only primitive types (floats) we don't need to free up any resources so we don't need a destructor

	// Accessor methods - For this implementation, we'll provide accessors, but since x, y accept all float values we don't need validation so use of accessors is optional.  (x, y) can be accessed directly from the caller since they're declared as public above
	float getX();
	float getY();
	void setX(const float value);
	void setY(const float value);
};