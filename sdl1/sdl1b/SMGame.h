#pragma once

#include <SDL.h>
#include "Clock.h"

class SMScene;

//
// Model the host game - only one instance will (should) be created - this is an example of a Singleton
//

class SMGame {

private:

	// The game object will own the window and renderer objects
	SDL_Window*		gameWindow = nullptr;
	SDL_Renderer*	gameRenderer = nullptr;

	// In addition, the game object will also store the scenes that make up the game...
	SMScene*		scenes[1]; // ...for now just a single one - our game!
	Clock * gameClock = nullptr;

public:

	// Constructor - setup SDL window and renderer based on the given size (w, h) and flags (initFlags and windowFlags)
	SMGame(Uint32 initFlags, char *windowTitle, int w, int h, Uint32 windowFlags);
	~SMGame();
	void _init_(Uint32 initFlags, char *windowTitle, int w, int h, Uint32 windowFlags);
	// SMGame has a single method - start.  This essentially calls the run method of the first scene to show.
	void start(void);

};