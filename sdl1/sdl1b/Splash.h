#pragma once
#include "textObject.h"
#include "GameObject.h"
#include <SDL_ttf.h>
#include <stdlib.h>
#include "GameObject.h"
#include <cstdlib>
#include <string>
#include <string.h>
#include "Point2f.h"

class Splash :
	public textObject
{
public:
	Splash(char*, Point2f, SDL_Renderer*, float, SDL_Color, int, Point2f, int);
	~Splash();
	void update(KeyFlags keys, int framerate);
	bool doneCheck() {  return done;}
private:
	char* text;
	float first;
	float timeOn;
	float currTrans;
	int peakTrans = 255;
	float sixth;
	bool firstb  = true;
	bool flipflop = false;
	int alphaPerSec; 
	float timePassed;
	bool done = false;

};

