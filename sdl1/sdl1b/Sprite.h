#pragma once
#include <Windows.h>
#include <SDL.h>
#include <iostream>
#include <stdio.h>
class Sprite
{
private:
	SDL_Rect position;
	SDL_Texture* texture;


	int textMax = 1;
	int currText = 0;

public:
	Sprite();
	~Sprite();
	
	SDL_Rect GetRect();
	SDL_Texture * GetText();

	void SetRect(int, int, int, int);
	void SetText(SDL_Renderer*, char*);

};

