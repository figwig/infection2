#pragma once
#include "GameObject.h"
#include "Point2f.h"
#include "CircleVol.h"
#include "Sprite.h"
#include "Bullet.h"
#include "Player.h"

class Virus :
	public GameObject
{
	private:

	Point2f				pos; // the virus's position in the world

	float				theta;

	Sprite				virusSprite;

	const float			SIZE = 0.15f;

	CircleVol			virusVolume;

	boolean				collided;

	Point2f				centrePos;
	int					count = 0;

	int age = 0;

	int					health = 10;
	
	int					framerate = 1;// inital value for framerate

	SDL_Rect			targetRect;


public:
	Virus(Point2f initialPosition, char* sourceImage, SDL_Renderer* renderer);
	~Virus();
	// Update the player - we call this every iteration of the main game loop
	void update(KeyFlags keys, int framerate);

	// Draw the player - we call this every iteration of the main game loop
	void draw(SDL_Renderer* renderer);
	void move(float, float);
	void moveTow(float, float, float);
	Volume* getVolumePtr();

 
	void flipvelocity(GameObject*);

  	void letMove();

	Point2f getPos();

	int getHealth() { return health; }

	int getDamagePlayer() { return 1; }

	int getDamageBullet() { return 4; }

	int getAge() { return age; }

};



