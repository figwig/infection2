#pragma once
#include "GameObject.h"
#include "Collision.h"
#include "Point2f.h"

class Volume
{
public:
	Volume() {}
	~Volume() {}
	virtual bool checkCollision(Point2f) = 0;
	virtual Point2f* getPoints() = 0;
	virtual void createPoints(float) = 0;
	virtual int getNumberOfPoints() = 0;
	virtual void updatePos(int, int) = 0;	
	virtual void setOwner(GameObject*) = 0;
	virtual GameObject* getOwner() = 0;

protected:
	bool passive;
	GameObject* owner;

};

