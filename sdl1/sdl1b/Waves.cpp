#include "GameScene.h"
#include <SDL.h>
#include "SMGame.h"
#include "Player.h"
#include "Background.h"
#include "Virus.h"
#include <string>
#include "Volume.h"
#include "fpsCounter.h"
#include "point.h"
#include "staticText.h"
#include "Splash.h"
#include "GameObject.h"


void GameScene::startWave(int waveNo)//called everytime that the newWaveTrigger is called.
{
	if (waveNo == 0)
	{
		gamePlaying = false;
		//spawn like a menu
		currWave = 1;
		createObjects();


		addToDrawables(new Splash("Wave 1!", Point2f(800.0f, 420.0f), renderer, 2.0f, red, 3, Point2f(300.0f, 40.0f), 255));
	}

	else if (waveNo == 1)
	{	//called, so create the first wave
		gamePlaying = true;
		newWaveTrigger = false;

		player = new Player(Point2f(866.0f, 364.0f), "Assets\\Images\\player.png", renderer, this);
		volumes[0] = (player->getVolumePtr());
		createPlayerDebug();
		createDebugPoints();
		initGame();

		//wave parameters
		frameDelay = 3; //delay before start of spawn in ms
		interval = 3;// interval in ms
		levelSpawns = 5;// how many viruses in this wave

		currWave = 2;

	}


	else if (waveNo == 2)// setup wave 2
	{

		addToDrawables(new Splash("Wave 2!", Point2f(800.0f, 420.0f), renderer, 2.0f, red, 3, Point2f(300.0f,40.0f), 255));
		newWaveTrigger = false;
		spawned = 0;
		gamePlaying = true;


		//wave parameters
		frameDelay = 4; //delay before start of spawn in seconds
		interval = 3;// interval in seconds
		levelSpawns = 15;// how many viruses in this wave

		currWave = 3;


	}
	else if (waveNo == 3)
	{
		addToDrawables(new Splash("Wave 3!", Point2f(800.0f, 420.0f), renderer, 2.0f, red, 3, Point2f(300.0f, 40.0f), 255));
		newWaveTrigger = false;
		spawned = 0;
		gamePlaying = true;


		//wave parameters
		frameDelay = 4; //delay before start of spawn in seconds
		interval = 3;// interval in seconds
		levelSpawns = 15;// how many viruses in this wave

		currWave = 4;
	}
	else if (waveNo == 4)
	{
		addToDrawables(new Splash("Congratulations!", Point2f(800.0f, 420.0f), renderer, 2.0f, red, 3, Point2f(300.0f, 40.0f), 255));
		currWave = 99;
		newWaveTrigger = false;
		frameDelay = 4; //delay before start of spawn in seconds
		interval = 3;// interval in seconds
		levelSpawns = 0;// how many viruses in this wave

	}
	else if (waveNo == 98)
	{
		frameDelay = 4; //delay before start of spawn in seconds
		interval = 3;// interval in seconds
		levelSpawns = 0;// how many viruses in this wave
		addToDrawables(new Splash("You died!", Point2f(800.0f, 420.0f), renderer, 5, purp, 3, Point2f(300.0f, 50.0f), 255));
		currWave = 99;
		newWaveTrigger = false;
	}
	else if (waveNo == 99)
	{
		quitGame = true;
	}

}
