#pragma once
#include "textObject.h"
class fpsCounter :public textObject
{
public:
	fpsCounter(Point2f initialPosition, SDL_Renderer* renderer);
	~fpsCounter();
	void update(KeyFlags, int);
	void draw(SDL_Renderer*);
	Point2f				pos;
	void flipvelocity(int) {};

	Point2f getPos() { return Point2f(0, 0); };

private:
	SDL_Texture* Message;
	SDL_Surface* surfaceMessage;
	SDL_Color White;
	TTF_Font* Sans;
	std::string printOut;
	char *cstr;
};

