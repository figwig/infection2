
#include <Windows.h>
#include <SDL.h>
#include <iostream>
#include <stdio.h>
#include "SMGame.h"


using namespace std;


int main(int argc, char* args[]) {

	// Our project is setup to use the Windows subsystem, so we don't get a console window by default.  AllocConsole creates one for us and freopen redirects stdout to the new console (so anything we print with cout or printf goes to the new console).  Useful for debugging!
	AllocConsole();
	freopen("CON", "w", stdout);

	// Create a new game object (there can be only one!)
	SMGame *game = new SMGame(SDL_INIT_EVERYTHING, "Infection", 1920, 1080, SDL_WINDOW_SHOWN);

	// If valid call start on the game object, otherwise return '1' as an error code
	if (game != nullptr) {

		// Start game - we don't leave this method until we quit the game
		game->start();

		// We're done - delete game and clean-up in the SMGame constructor
		delete game;
		return 0;
	}
	else {

		return 1; // Indicate some error
	}
}
