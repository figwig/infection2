#pragma once
#include "GameObject.h"
#include "Sprite.h"
#include "Point2f.h"
class point :
	public GameObject
{

private:
	Point2f pos; //position on screen
	Sprite pointSprite;
	const float SIZE = 1.0f;
	Volume* owner;
	
public:
	point(Point2f initialPosition, char* sourceImage, SDL_Renderer* renderer);
	~point();
	void draw(SDL_Renderer*);
	void update(KeyFlags, int framerate);
	void flipvelocity(int);// does not actually flip the velocity, in fact sends the object the current speed away from the other object ot stops it
	Point2f getPos();
	void updatePos(Point2f);

	void setOwner(Volume*newOwner)	{ owner = newOwner; }
	Volume* getOwner()			    { return owner; }

};

