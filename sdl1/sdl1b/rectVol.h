#pragma once
#include "Volume.h"
#include "staticText.h"
class rectVol :
	public Volume
{
public:
	rectVol();
	~rectVol();
	void _init_(float newheight, float newwidth, Point2f newcentre, bool ispassive, float delta);
	bool checkCollision(Point2f);
	Point2f*  getPoints();
	void createPoints(float);
	int getType();
	float getWidth();
	float getHeight();
	int getNumberOfPoints();
	void updatePos(int,int);
	void setOwner(GameObject*);
	GameObject* getOwner();

private:
	float width;
	float height;
	Point2f centre;
	bool passive;
	static const int NO_OF_POINTS = 132;
	Point2f keyPoints[NO_OF_POINTS];
	GameObject* owner;
	int actNumPoints;
	float delta;
	float newX;
	float newY;
	

};

