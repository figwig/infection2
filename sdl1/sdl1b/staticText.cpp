#include "staticText.h"
#include "fpsCounter.h"
#include "textObject.h"
#include "GameObject.h"
#include <SDL_ttf.h>
#include <stdlib.h>
#include "GameObject.h"
#include <cstdlib>
#include <string.h>
#include "Point2f.h"
#include <stdio.h>
#include <string>
#include <SDL_image.h>


staticText::staticText(Point2f initialPosition, SDL_Renderer* renderer)
{
	TTF_Init();
	Sans = TTF_OpenFont("Assets\\Fonts\\OpenSans-Regular.ttf", 24); //this opens a font style and sets a size

	pos = Point2f(initialPosition.x, initialPosition.y);

	White = { 20, 205, 50 }; 
	printOut = "default";


	Message_rect.x = pos.x;
	Message_rect.y = pos.y;
	Message_rect.w = 150;
	Message_rect.h = 30;
}


staticText::~staticText()
{

}

void staticText::update(KeyFlags keys, int score)
{
	//getting the fps into a string format so it can be turned into a texture
	printOut = tempPrintOut;
	printOut = printOut + std::to_string(score);
	cstr = new char[printOut.length() + 1];
	strcpy(cstr, printOut.c_str());

	surfaceMessage = TTF_RenderText_Solid(Sans, cstr, White);
	free(cstr);
}

void staticText::draw(SDL_Renderer * renderer)
{
	Message = SDL_CreateTextureFromSurface(renderer, surfaceMessage);




	SDL_RenderCopyEx(renderer, Message, NULL, &Message_rect, 0, 0, SDL_FLIP_NONE);
}

void staticText::setPrint(char * text)
{
	tempPrintOut = text;
}

